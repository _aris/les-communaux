<?php
if ( !defined( 'ABSPATH' ) ) exit;

// Enqueue parent style
add_action( 'wp_enqueue_scripts', 'communaux_theme_parent_css', 10 );
function communaux_theme_parent_css() {
    wp_enqueue_style( 'twentytwenty-parent-style', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }

// Unregister WordPress Core block patterns
add_action('init', function() {
    remove_theme_support('core-block-patterns');
},  9  );

// Unregister Twenty Twenty block patterns
add_action( 'init', 'communaux_theme_unregister_tt_patterns' );
function communaux_theme_unregister_tt_patterns() {
    unregister_block_pattern( 'twentytwenty/event-details' );
    unregister_block_pattern( 'twentytwenty/introduction' );
}

// More Social Icons
add_filter( 'twentytwenty_social_icons_map', 'communaux_theme_tt_social_icons_map' );
function communaux_theme_tt_social_icons_map( $icons ) {
	$icons['fediverse'] = array(
		'mamot.fr',
	);
	$icons['telegram'] = array(
		't.me',
	);
	return $icons;
}
add_filter( 'twentytwenty_svg_icons_social', 'communaux_theme_tt_svg_icons_social' );
function communaux_theme_tt_svg_icons_social( $icons ) {
	$icons['fediverse'] = '<svg width="21" height="21" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg"><path d="M23.193 7.879c0-5.206-3.411-6.732-3.411-6.732C18.062.357 15.108.025 12.041 0h-.076c-3.068.025-6.02.357-7.74 1.147 0 0-3.411 1.526-3.411 6.732 0 1.192-.023 2.618.015 4.129.124 5.092.934 10.109 5.641 11.355 2.17.574 4.034.695 5.535.612 2.722-.15 4.25-.972 4.25-.972l-.09-1.975s-1.945.613-4.129.539c-2.165-.074-4.449-.233-4.799-2.891a5.499 5.499 0 0 1-.048-.745s2.125.52 4.817.643c1.646.075 3.19-.097 4.758-.283 3.007-.359 5.625-2.212 5.954-3.905.517-2.665.475-6.507.475-6.507zm-4.024 6.709h-2.497V8.469c0-1.29-.543-1.944-1.628-1.944-1.2 0-1.802.776-1.802 2.312v3.349h-2.483v-3.35c0-1.536-.602-2.312-1.802-2.312-1.085 0-1.628.655-1.628 1.944v6.119H4.832V8.284c0-1.289.328-2.313.987-3.07.68-.758 1.569-1.146 2.674-1.146 1.278 0 2.246.491 2.886 1.474L12 6.585l.622-1.043c.64-.983 1.608-1.474 2.886-1.474 1.104 0 1.994.388 2.674 1.146.658.757.986 1.781.986 3.07v6.304z"/></svg>';
	$icons['telegram'] = '<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32">
    <path d="M16 .5C7.437.5.5 7.438.5 16S7.438 31.5 16 31.5c8.563 0 15.5-6.938 15.5-15.5S24.562.5 16 .5zm7.613 10.619l-2.544 11.988c-.188.85-.694 1.056-1.4.656l-3.875-2.856-1.869 1.8c-.206.206-.381.381-.781.381l.275-3.944 7.181-6.488c.313-.275-.069-.431-.482-.156l-8.875 5.587-3.825-1.194c-.831-.262-.85-.831.175-1.231l14.944-5.763c.694-.25 1.3.169 1.075 1.219z"/>
    </svg>';
	return $icons;
}

