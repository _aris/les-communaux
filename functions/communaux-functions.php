<?php
/*
Plugin Name:  Les Communaux - Fonctions
Plugin URI:   https://codeberg.org/_aris/les-communaux
Description:  Personalisation et configuration pour le site des Communaux
Author:       aris~
Author URI:   http://communaux.cc
License:      GNU General Public License v2 or later
License URI:  http://www.gnu.org/licenses/gpl-2.0.txt
Version:      2022.08.11
*/

if(!defined('ABSPATH')) exit; // Exit if accessed directly

/* 
 * WordPress Headers 
 * ------------------------------------- 
 */

add_filter('the_generator', 'tmprs_remove_version');
function tmprs_remove_version() {return '';}
add_filter('style_loader_src', 'tmprs_remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'tmprs_remove_version_scripts_styles', 9999);
function tmprs_remove_version_scripts_styles($src){if(strpos($src,'ver=')){$src=remove_query_arg('ver',$src);}return $src;}

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);

/* 
 * Theme Support 
 * ------------------------------------- 
 */

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

add_action('wp_enqueue_scripts', 'tmprs_deregister_jquery');
function tmprs_deregister_jquery() { if ( !is_admin() ) {wp_deregister_script('jquery');} } 
add_action('after_setup_theme', 'tmprs_remove_post_format', 15);
function tmprs_remove_post_format() { remove_theme_support('post-formats'); }

/* 
 * Content 
 * ------------------------------------- 
 */

add_filter( 'get_the_archive_title', 'tmprs_remove_archive_title_prefix' );
function tmprs_remove_archive_title_prefix( $title ) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif ( is_year() ) {
		$title = get_the_date( 'Y' );
	} elseif ( is_month() ) {
		$title = get_the_date( 'F Y' );
	} elseif ( is_day() ) {
		$title = get_the_date( get_option( 'date_format' ) );
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_tax() ) {
		$title = single_term_title( '', false );
	} elseif ( is_search() ) {
		$title = '&lsquo;' . get_search_query() . '&rsquo;';
	} else {
		$title = __( 'Archives', 'twentytwenty-child' );
	} // End if().
	return $title;
}

add_action('template_redirect',function(){if(isset($_GET['author'])||is_author()){global $wp_query;$wp_query->set_404();status_header(404);nocache_headers();}},1);
add_filter('user_row_actions',function($actions){if(isset($actions['view']))unset($actions['view']);return $actions;},PHP_INT_MAX,2);
add_filter( 'author_link', function() { return '#'; }, PHP_INT_MAX );
add_filter( 'the_author_posts_link', '__return_empty_string', PHP_INT_MAX );

add_filter( 'rewrite_rules_array', 'tmprs_cleanup_attachment_permalink' );
function tmprs_cleanup_attachment_permalink( $rules ) {
	foreach ( $rules as $regex => $query ) {
		if ( strpos( $regex, 'attachment' ) || strpos( $query, 'attachment' ) ) {
			unset( $rules[ $regex ] );
		}
	}

	return $rules;
}

add_filter( 'attachment_link', 'tmprs_cleanup_attachment_link' );
function tmprs_cleanup_attachment_link( $link ) {
	return;
}

add_filter( 'the_excerpt_rss', 'tmprs_thumbnails_in_rss' );
add_filter( 'the_content_feed', 'tmprs_thumbnails_in_rss' );
function tmprs_thumbnails_in_rss( $content ) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ) {
		$content = '<figure>' . get_the_post_thumbnail( $post->ID, 'medium' ) . '</figure>' . $content;
	}
	return $content;
}

function tmprs_publish_later_on_feed($where) {
    global $wpdb;
    if ( is_feed() ) {
        $now = gmdate('Y-m-d H:i:s');
        // value for wait; + device
        $wait = '10'; // integer
        $device = 'MINUTE'; 
        $where .= " AND TIMESTAMPDIFF($device, $wpdb->posts.post_date_gmt, '$now') > $wait ";
    }
    return $where;
}
add_filter('posts_where', 'tmprs_publish_later_on_feed');

/* 
 * Admin UI
 * ------------------------------------- 
 */

add_filter( 'show_admin_bar', '__return_false' );
remove_action('welcome_panel', 'wp_welcome_panel');

add_action( 'admin_init', 'tmprs_remove_dashboard_meta' ); 
function tmprs_remove_dashboard_meta() {
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
	remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}

function tmprs_remove_screen_options() { if(!current_user_can('manage_options')) { return false;} return true; }
add_filter('screen_options_show_screen', 'tmprs_remove_screen_options');

add_filter('admin_footer_text', 'tmprs_change_footer_admin');
function tmprs_change_footer_admin () { echo 'Welcome to the Frontline!'; }

add_action( 'wp_before_admin_bar_render', 'tmprs_remove_logo_wp_admin', 0 );
function tmprs_remove_logo_wp_admin() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wp-logo' );
}

add_filter('send_password_change_email', '__return_false');
remove_action('after_password_reset', 'wp_password_change_notification');

add_filter('user_contactmethods','tmprs_contact_methods',10,1);
function tmprs_contact_methods($contactmethods) {
   unset($contactmethods['aim']);
   unset($contactmethods['jabber']);
   unset($contactmethods['yim']);
   return $contactmethods;
}

/* 
 * Security & Spam 
 * ------------------------------------- 
 */

add_filter('xmlrpc_enabled', '__return_false'); // Warning -> WP desktop Apps will not work
add_filter('wp_is_application_passwords_available', '__return_false');  // Warning -> WP Mobiles Apps will not work
add_filter( 'login_errors', 'tmprs_no_wordpress_errors' );
function tmprs_no_wordpress_errors(){ return 'Houston, nous avons un problème...'; }
add_filter ( 'allow_password_reset', 'tmprs_disable_password_reset' );
function tmprs_disable_password_reset() { return false; }
add_action('pre_ping','tmprs_no_self_ping');
function tmprs_no_self_ping(&$links){$home=get_option('home');foreach($links as $l=>$link)if(0===strpos($link,$home))unset($links[$l]);}

if (!is_admin()) {
	if (preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'])) die('No Way!');
	add_filter('redirect_canonical', 'tmprs_check_enum', 10, 2);
}
function tmprs_check_enum($redirect, $request) {
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request)) die();
	else return $redirect;
}

/* 
 * Plugins 
 * ------------------------------------- 
 */

// Share on Mastodon - Tested with 0.6.4
add_filter( 'share_on_mastodon_cutoff', '__return_true' );
add_filter( 'share_on_mastodon_attached_images', '__return_false' );
add_filter( 'share_on_mastodon_status', function( $status, $post ) {
    $status  = get_the_title( $post ) . "\n\n";
    //$status .= apply_filters( 'the_excerpt', get_the_excerpt( $post ) ) . "\n\n";
    $status .= get_permalink( $post );
    $status = wp_strip_all_tags(
        html_entity_decode( $status, ENT_QUOTES | ENT_HTML5, get_bloginfo( 'charset' ) )
    );
    return $status;
}, 10, 2 );
