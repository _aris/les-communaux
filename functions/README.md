Plugin de Configuration de WordPress pour le site [communaux.cc](https://communaux.cc).

Celui-ci comporte un ensemble de fonctions personalisées et de _hooks_ qui permettent :

- Une modification de fonctionalités native du CMS (_WordPress Core_).
- L’ajout de fonctionalités non prisent en charge nativement par le CMS.
- La configuration avancée de certains plugins utilisés par ailleurs.

Le code de ce plugin de comporte _aucune_ dépendance envers des ressources externes.