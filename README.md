# Les communaux

Fichiers et scripts de personnalisation pour le site Web communaux.cc

**custom/**   
Types de contenus (Post Type) et taxonomies personnalisées pour le site des Communaux.

**functions/**   
Plugin WordPress de fonctions personnalisées (_backend/frontend_).

**Theme/**   
Thème enfant (_child theme_) qui permet de surcharger le thème Twenty Twenty.

### Développement

Compatibilité testée avec:

- WordPress version 5.9 et 6.0
- PHP version 7.3 et 7.4

Pour le thème :

- Twenty twenty 2.0
- Twentig 1.4.6

### License

GNU General Public License v2 or later   
[http://www.gnu.org/licenses/gpl-2.0.txt](http://www.gnu.org/licenses/gpl-2.0.txt)
