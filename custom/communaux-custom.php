<?php
/*
Plugin Name:  Les Communaux - Types de contenus
Plugin URI:   https://codeberg.org/_aris/les-communaux
Description:  Types de contenus (Custom Post Type) et taxonomies personnalisées pour le site des Communaux.
Author:       aris~
Author URI:   https://papatheodorou.net
Version:      2022.08.11
License:      GNU General Public License v2 or later
License URI:  http://www.gnu.org/licenses/gpl-2.0.txt
*/

add_action( 'init', 'communaux_register_post_type' );
function communaux_register_post_type() {
	$labels = [
		'name'                     => esc_html__( 'Contributions', 'communaux' ),
		'singular_name'            => esc_html__( 'Contribution', 'communaux' ),
		'add_new'                  => esc_html__( 'Ajouter', 'communaux' ),
		'add_new_item'             => esc_html__( 'Ajouter une contribution', 'communaux' ),
		'edit_item'                => esc_html__( 'Editer la contribution', 'communaux' ),
		'new_item'                 => esc_html__( 'Nouvelle contribution', 'communaux' ),
		'view_item'                => esc_html__( 'Voir la contribution', 'communaux' ),
		'view_items'               => esc_html__( 'Voir les contributions', 'communaux' ),
		'search_items'             => esc_html__( 'Rechercher des contributions', 'communaux' ),
		'not_found'                => esc_html__( 'Aucune contribution trouvée', 'communaux' ),
		'not_found_in_trash'       => esc_html__( 'Aucune contribution à la corbeille', 'communaux' ),
		'parent_item_colon'        => esc_html__( 'Contribution parente:', 'communaux' ),
		'all_items'                => esc_html__( 'Toutes les contributions', 'communaux' ),
		'archives'                 => esc_html__( 'Archive des contribution', 'communaux' ),
		'attributes'               => esc_html__( 'Contribution ', 'communaux' ),
		'insert_into_item'         => esc_html__( 'Insérer dans la contribution', 'communaux' ),
		'uploaded_to_this_item'    => esc_html__( 'Uploaded to this contribution', 'communaux' ),
		'featured_image'           => esc_html__( 'Image mise en avant', 'communaux' ),
		'set_featured_image'       => esc_html__( 'Définir l’image mise en avant', 'communaux' ),
		'remove_featured_image'    => esc_html__( 'Supprimer l’image mise en avant', 'communaux' ),
		'use_featured_image'       => esc_html__( 'Utiliser comme image mise en avant', 'communaux' ),
		'menu_name'                => esc_html__( 'Contributions', 'communaux' ),
		'filter_items_list'        => esc_html__( 'Filter la liste des contributions', 'communaux' ),
		'filter_by_date'           => esc_html__( 'Filtrer par date', 'communaux' ),
		'items_list_navigation'    => esc_html__( 'Contributions list navigation', 'communaux' ),
		'items_list'               => esc_html__( 'Liste des contributions', 'communaux' ),
		'item_published'           => esc_html__( 'Contribution publiée', 'communaux' ),
		'item_published_privately' => esc_html__( 'Contribution publiée en mode privé', 'communaux' ),
		'item_reverted_to_draft'   => esc_html__( 'Contribution enregistrée comme brouillon', 'communaux' ),
		'item_scheduled'           => esc_html__( 'Contribution programmée', 'communaux' ),
		'item_updated'             => esc_html__( 'Contribution mise à jour', 'communaux' ),
		'text_domain'              => esc_html__( 'communaux', 'communaux' ),
	];
	$args = [
		'label'               => esc_html__( 'Contributions', 'communaux' ),
		'labels'              => $labels,
		'description'         => 'Articles et documents relatifs au projet des Communaux',
		'public'              => true,
		'hierarchical'        => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'show_in_rest'        => true,
		'query_var'           => true,
		'can_export'          => true,
		'delete_with_user'    => false,
		'has_archive'         => true,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'menu_icon'           => 'dashicons-text',
		'menu_position'       => 5,
		'capability_type'     => 'post',
		'supports'            => ['title', 'editor', 'thumbnail', 'excerpt'],
		'taxonomies'          => ['category', 'post_tag'],
		'rewrite'             => [
			'with_front' => false,
		],
	];

	register_post_type( 'contributions', $args );
}

// Taxonomie
add_action( 'init', 'communaux_register_taxonomy' );
function communaux_register_taxonomy() {
	$labels = [
		'name'                       => esc_html__( 'Chantiers', 'communaux' ),
		'singular_name'              => esc_html__( 'Chantier', 'communaux' ),
		'menu_name'                  => esc_html__( 'Chantiers', 'communaux' ),
		'search_items'               => esc_html__( 'Rechercher dans les chantiers', 'communaux' ),
		'popular_items'              => esc_html__( 'Chantiers populaire', 'communaux' ),
		'all_items'                  => esc_html__( 'Tous les chantiers', 'communaux' ),
		'parent_item'                => esc_html__( 'Chantier parent', 'communaux' ),
		'parent_item_colon'          => esc_html__( 'Parent ', 'communaux' ),
		'edit_item'                  => esc_html__( 'Modifier le chantier', 'communaux' ),
		'view_item'                  => esc_html__( 'Voir le chantier', 'communaux' ),
		'update_item'                => esc_html__( 'Mettre à jour le chantier', 'communaux' ),
		'add_new_item'               => esc_html__( 'Ajouter un chantier', 'communaux' ),
		'new_item_name'              => esc_html__( 'Nouveau nom du chantier', 'communaux' ),
		'separate_items_with_commas' => esc_html__( 'Séparer les noms de chantiers par des virgules', 'communaux' ),
		'add_or_remove_items'        => esc_html__( 'Ajouter ou supprimer des chantiers', 'communaux' ),
		'choose_from_most_used'      => esc_html__( 'Choisir à partir des chantiers les plus utilisés', 'communaux' ),
		'not_found'                  => esc_html__( 'Aucun chantier trouvé', 'communaux' ),
		'no_terms'                   => esc_html__( 'Aucun chantier', 'communaux' ),
		'filter_by_item'             => esc_html__( 'Filtrer par chantier', 'communaux' ),
		'items_list_navigation'      => esc_html__( 'Chantiers list pagination', 'communaux' ),
		'items_list'                 => esc_html__( 'Chantiers list', 'communaux' ),
		'most_used'                  => esc_html__( 'Les plus utilisés', 'communaux' ),
		'back_to_items'              => esc_html__( 'Retour à la page des chantiers', 'communaux' ),
		'text_domain'                => esc_html__( 'communaux', 'communaux' ),
	];
	$args = [
		'label'              => esc_html__( 'Chantiers', 'communaux' ),
		'labels'             => $labels,
		'description'        => '',
		'public'             => true,
		'publicly_queryable' => true,
		'hierarchical'       => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus'  => true,
		'meta_box_cb'        => true,
		'show_in_rest'       => true,
		'show_tagcloud'      => false,
		'show_in_quick_edit' => true,
		'show_admin_column'  => false,
		'query_var'          => true,
		'sort'               => false,
		'rest_base'          => '',
		'rewrite'            => [
			'with_front'   => false,
			'hierarchical' => false,
		],
	];
	register_taxonomy( 'chantier', ['post', 'contributions'], $args );
}

// Support Tags & Categories for Custom Post Type (contributions)
add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if( is_category() || is_tag() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'contributions'); // don't forget nav_menu_item to allow menus to work!
    $query->set('post_type',$post_type);
    return $query;
    }
}

// Display Post Type in Dashboard
add_filter( 'dashboard_glance_items', 'communaux_glance_items', 10, 1 );
function communaux_glance_items( $items = array() ) {
    $post_types = array( 'contributions' );
    foreach( $post_types as $type ) {
        if( ! post_type_exists( $type ) ) continue;
        $num_posts = wp_count_posts( $type );
        if( $num_posts ) {
            $published = intval( $num_posts->publish );
            $post_type = get_post_type_object( $type );
            $text = _n( '%s ' . $post_type->labels->singular_name, '%s ' . $post_type->labels->name, $published, 'your_textdomain' );
            $text = sprintf( $text, number_format_i18n( $published ) );
            if ( current_user_can( $post_type->cap->edit_posts ) ) {
            $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $text . '</a>';
                echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
            } else {
            $output = '<span>' . $text . '</span>';
                echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
            }
        }
    }
    return $items;
}

// Support of Newspack Homepage Blocks - Pas utilisé
// https://developer.wordpress.org/reference/functions/add_post_type_support/
// https://newspack.pub/support/blocks/homepage-posts/
// add_post_type_support( 'contributions', 'newspack_blocks' );
